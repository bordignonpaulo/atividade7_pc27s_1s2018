
Exemplo simples de intera��o entre Cliente e Servidor RMI
---------------------------------------------------------

Autor: Paulo Bordignon
Data: 25/04/2018

Arquivos necess�rios para o lado servidor:
- Servidor.class
- IServidor.class

Arquivos necess�rios para o lado cliente:
- Cliente.class
- IServidor.class

Esse exemplo considera que exista uma interface comum (IServidor.class)
que define quais m�todos do objeto servidor podem ser acessados remotamente
pelo objeto cliente

Os arquivos cliente.policy e servidor.policy definem as permiss�es de seguran�a
necess�rias para o acesso remoto. 

------------
Para compilar:
javac -cp . *.java


Execu��o:

export HOSTNAME=$HOSTNAME

SERVIDOR:
java -cp . -Djava.security.policy=servidor.policy Servidor

CLIENTE:
java -cp . -Djava.security.policy=cliente.policy Cliente <ENDERE�O_IP> <VALOR_INTEIRO>
