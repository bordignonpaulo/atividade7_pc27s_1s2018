
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class Servidor extends UnicastRemoteObject implements IServidor {
	
	private static final long serialVersionUID = 1L;
	private static final int PORTA_REGISTRO = 1099;
	private static final String NOME_SERVICO = "SERVIDOR_RMI";


	public Servidor() throws RemoteException {
		//Eh necessario ter um construtor aqui para
		//invocar o metodo construtor da superclasse
		super();		
	}	

	public static void main(String args[]){		
		
		//Gerenciador de Seguranca
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
		
		try {
			//Criar o registry na porta especificada
			Registry registry = LocateRegistry.createRegistry(PORTA_REGISTRO);
			
			//Criar o servidor
			Servidor servidor = new Servidor();
			
			//Registrar o servidor
			registry.rebind(NOME_SERVICO, servidor);
			
			System.out.println("Servidor RMI em " + System.getenv("HOSTNAME") + " ativado.");
			
		} catch ( Exception e ){
			
			System.out.println(e.getMessage());			
			
		}//fim catch
		
	}//fim main
       

	public String executarTarefa(String args) throws RemoteException {		

	    String resultado = "";

	    //Se o cliente fornecer como argumento a string 1
	    if (args.equals("1")){
                int converter = Integer.parseInt(args);
                int soma = converter+10; 
		resultado = "Primeira opcao. Valor recebido do SERVIDOR= " + soma;
                System.out.println("VALOR IGUAL A 1 FOI RECEBIDO DO CLIENTE");
            }else{
                int converter = Integer.parseInt(args);
                int soma = converter+10; 
		resultado = "Segunda opcao. Valor recebido do SERVIDOR= " + soma;
                System.out.println("VALOR DIFERENTE DE 1 FOI RECEBIDO DO CLIENTE");
            }
	    return resultado;
	
    }//fim executarTarefa
	
        // NOVOS METODOS
        
        public String repetirNumero(String args) throws RemoteException {		

	    String resultado = "";

	    //Se o cliente fornecer como argumento a string 1
	    if (args.equals("1")){
                
		resultado = "valor digitado= " + args;
                //System.out.println("VALOR IGUAL A 1 FOI RECEBIDO DO CLIENTE");
            }else{
               
		resultado = "valor digitado= " + args;
                //System.out.println("VALOR DIFERENTE DE 1 FOI RECEBIDO DO CLIENTE");
            }
	    return resultado;
	
    }//fim repetirNumero
        
        
        public String repetirNumeroM2(String args) throws RemoteException {		

	    String resultado = "";

	    //Se o cliente fornecer como argumento a string 1
	    if (args.equals("1")){
                int converter = Integer.parseInt(args);
                int soma = converter*2; 
		resultado = "valor x 2= " + soma;
                //System.out.println("VALOR IGUAL A 1 FOI RECEBIDO DO CLIENTE");
            }else{
                int converter = Integer.parseInt(args);
                int soma = converter*2; 
		resultado = "valor x 2= " + soma;
                //System.out.println("VALOR DIFERENTE DE 1 FOI RECEBIDO DO CLIENTE");
            }
	    return resultado;
	
    }//fim repetirNumeroM2
        
        
        
        public String repetirNumeroM4(String args) throws RemoteException {		

	    String resultado = "";

	    //Se o cliente fornecer como argumento a string 1
	    if (args.equals("1")){
                int converter = Integer.parseInt(args);
                int soma = converter*4; 
		resultado = "valor x 4= " + soma;
                //System.out.println("VALOR IGUAL A 1 FOI RECEBIDO DO CLIENTE");
            }else{
                int converter = Integer.parseInt(args);
                int soma = converter*4; 
		resultado = "valor x 4= " + soma;
                //System.out.println("VALOR DIFERENTE DE 1 FOI RECEBIDO DO CLIENTE");
            }
	    return resultado;
	
    }//fim repetirNumeroM4
        
        
        
}//fim Servidor
