
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface IServidor extends Remote {

	public String executarTarefa(String argumentos) throws RemoteException;
	
        // novos metodos
        public String repetirNumero(String argumentos) throws RemoteException;
        
        public String repetirNumeroM2(String argumentos) throws RemoteException;
        
        public String repetirNumeroM4(String argumentos) throws RemoteException;
        
}//fim interface
